# GitLab Runner S3 release verify test

Tests GPG signature verification of GitLab Runner S3 release.

## Author

Tomasz Maczukin, 2020, GitLab Inc.

## License

MIT

